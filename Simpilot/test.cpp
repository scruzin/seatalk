/*
  Description:
    Simpilot unit tests.
    #define TEST in port.h to compile.

  License:
    Copyright (C) 2016 Alan Noble.

    This file is part of Simpilot. Simpilot is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Simpilot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simpilot in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

#include "port.h"
#include "latlng.h"

#ifdef TEST
#define NauticalMile 1852
#define MsgSize 1000
#define MaxDigits 12

char PrintBuf[MsgSize];
#define PRINTLN(FMT, ...) { fmtString(PrintBuf, FMT, ##__VA_ARGS__), outputLine(PrintBuf); }
#define EQ_01(V1, V2, MSG) if (fabs(V1 - V2) < 0.01) PRINTLN(MSG ": pass") else PRINTLN(MSG ": FAIL (%f,%f)", V1, V2)
#define EQ_001(V1, V2, MSG) if (fabs(V1 - V2) < 0.001) PRINTLN(MSG ": pass") else PRINTLN(MSG ": FAIL (%f,%f)", V1, V2)

void test_formating() {
#ifdef MAIN
  printf("test: number formating\n");
  char buf[MaxDigits];
  size_t len;

   // test precision
  long ll = -1234567890;
  for (int precision = 0; precision < 11; precision++) {
    itoa(ll, 0, precision, buf, &len);
    printf("itoa(%ld,0,%d,...) = %s\n", ll, precision, buf);
  }

  // test padding
  ll = 12;
  for (int pad = 0; pad <= 5; pad++) {
    itoa(ll, pad, 0, (char*)buf , &len);
    printf("itoa(%ld,%d,0, ...) = %s, len=%lu\n", ll, pad, buf, len);
  }

  // misc tests
  double dd = .7;
  ll = long(pow(10, 1) * dd);
  itoa(ll, 0, 1, (char*)buf , &len);
  printf("dd=%f, itoa(%ld,0,1,...) = %s\n", dd, ll, buf);
  itoa(ll, 1, 1, (char*)buf , &len);
  printf("dd=%f, itoa(%ld,1,1,...) = %s\n", dd, ll, buf);

  double ee = 12.34;
  ll = long(100 * ee);
  itoa(ll, 2, 2, (char*)buf , &len);
  printf("ee=%f, itoa(%ld,2,2,...) = %s\n", ee, ll, buf);

  itoa(ll, 3, 1, (char*)buf , &len);
  printf("ee=%f, itoa(%ld,3,1,...) = %s\n", ee, ll, buf);

  itoa(ll, 3, 2, (char*)buf , &len);
  printf("ee=%f, itoa(%ld,3,2,...) = %s\n", ee, ll, buf);
#endif

  PRINTLN("test: formating");
  char fmtbuf[MsgSize];
  char cc = 'A';
  int ii = 12;
  double ff1 = 0.7;
  double ff2 = 12.3456789;
  fmtString(fmtbuf, "blah, %c, %d, %04d, %f, %.1f, %f, %.3f, %03.2f, %s, blah", cc, ii, ii, ff1, ff1, ff2, ff2, ff2, "Alan");
  if(strcmp(fmtbuf, "blah, A, 12, 0012, 0.700000, 0.7, 12.345679, 12.346, 012.35, Alan, blah") == 0) {
    PRINTLN("fmtString: pass");
  } else {
    PRINTLN("fmtString: FAIL");
  }
}

void  test_conversion() {
  // NB: 1 nautical mile = 1 minute of latitude = 1/60th (0.01666667) of a degree
  PRINTLN("test: conversion");
  int nm_sqrt2 = round(NauticalMile * sqrt(2));

  // trig
  double a1 = Geo::toRadians(90);
  EQ_001(a1, 1.5708, "toRadians");
  double a2 = Geo::fromRadians(a1);
  EQ_001(a2, 90, "fromRadians(a1)");

  double mile = Geo::toMeters(1, 'N');
  EQ_001(mile, NauticalMile, "toMeters(1, 'N')");
  double mile1 = Geo::fromMeters(mile, 'N');
  EQ_001(mile1, 1, "fromMeters(mile, 'N')");
  double mile2 = round(Geo::toMeters(sqrt(2), 'N'));
  EQ_001(mile2, nm_sqrt2, "toMeters(sqrt(2), 'N')");
}

void test_distances() {
  PRINTLN("test: distances and bearings");

  // small distances (using the the 1st 4 waypoints in KI_wineries.txt
  Geo::LatLng wpt1(-35.638,137.623);
  Geo::LatLng wpt2(-35.633,137.640);
  Geo::LatLng wpt3(-35.670,137.687);
  Geo::LatLng wpt4(-35.758,137.890);
  Geo::LatLng wpts[] = { wpt1, wpt2, wpt3, wpt4 };
  double wpt_dist[] = { 0, 0.8822, 3.1926, 11.2182 };
  double wpt_brng[] = { 0, 70.1106, 134.1061, 118.1571 };
  for (int ii = 1; ii < 4; ii++) {
    Geo::LatLng prev_wpt = wpts[ii - 1];
    Geo::LatLng this_wpt = wpts[ii];
    double dtw = Geo::fromMeters(prev_wpt.distance(this_wpt), 'N');
    double btw = prev_wpt.bearing(this_wpt);
    EQ_001(dtw, wpt_dist[ii], "wpt.distance()");
    EQ_01(btw, wpt_brng[ii], "wpt.bearing()");
  }

  // distances & bearings
  Geo::LatLng latlng(-35.0,137.0);
  Geo::LatLng latlng1(-35.0,143.0); // 6 degrees east (~90 degrees)
  Geo::LatLng latlng2(-27.0,137.0); // 8 degrees north (0 degrees)
  Geo::LatLng latlng3(-27.0,143.0); // north and east

  double dtw, btw;
  // large distances
  Geo::LatLng latlngs[] = { latlng1, latlng2, latlng3, };
  double dist[] = { 546.4311, 889.5594, 1056.9629 };
  double brng[] = { 91.7218, 0, 34.3309 };

  for (int ii = 0; ii < 3; ii++) {
    dtw = latlng.distance(latlngs[ii]);
    EQ_001(dtw/1000, dist[ii], "latlng.distance()");
    btw = latlng.bearing(latlngs[ii]);
    EQ_01(btw, brng[ii], "latlng.bearing()");
  }

  PRINTLN("test: cross track distances");
  // cross track distance
  Geo::LatLng pos1 = wpt1;
  Geo::LatLng pos2 = wpt2;
  Geo::LatLng pos3(-35.654, 137.655); // exactly half way between wpt1 and wpt3
  Geo::LatLng pos4 = wpt3;
  Geo::LatLng pos[] = { pos1, pos2, pos3, pos4 };
  double xtds[] = { 0, -1549.69, -40.35, 0 };
  for (int ii = 0; ii < 4; ii++) {
    double xtd = pos[ii].crossTrackDistance(wpt1, wpt3);
    EQ_01(xtd, xtds[ii], "crossTrackDistance()");
  }
}

void test_moves() {
  PRINTLN("test: moves");
  Geo::LatLng latlng1(-35.0,137.0);
  Geo::LatLng latlng2(-35.5,137.5);

  double dtw = latlng1.distance(latlng2);
  EQ_001(dtw/1000, 71.780856, "latlng2.move() distance");
  double btw = latlng1.bearing(latlng2);
  EQ_01(btw, 140.907517, "latlng2.move() bearing");
  EQ_001(latlng1.lat, -35.0, "before latlng1.lat");
  EQ_001(latlng1.lng, 137.0, "before latlng1.lng");
  latlng1.move(dtw, btw);
  dtw = latlng1.distance(latlng2);
  EQ_001(latlng1.lat, -35.5, "moved latlng1.lat");
  EQ_001(latlng1.lng, 137.5, "moved latlng1.lng");
  EQ_001(dtw/1000, 0.0, "latlng1.distance(latlng2)");

  double mile = NauticalMile;
  double mile2 = NauticalMile * sqrt(2);

  latlng1.set(0.0, 0.0);
  EQ_001(latlng1.lat, 0.0, "latlng1.set().lat");
  EQ_001(latlng1.lng, 0.0, "latlng1.set().lng");
  latlng1.move(mile,0); // 1 mile, due north
  EQ_001(latlng1.lat, 0.016655, "latlng1.move(mile,0).lat");
  EQ_001(latlng1.lng, 0.0, "latlng1.move(mile,0).lng");

  latlng1.set(0.0, 0.0);
  latlng1.move(mile, 90); // 1 mile, due east
  EQ_001(latlng1.lat, 0.0, "latlng1.move(mile,90).lat");
  EQ_001(latlng1.lng, 0.016655, "latlng1.move(mile,90).lng");

  latlng1.set(0.0, 0.0);
  latlng1.move(mile, 180); // 1 mile, due south
  EQ_001(latlng1.lat, -0.016655, "latlng1.move(mile,180).lat");
  EQ_001(latlng1.lng, 0.0, "latlng1.move(mile,180).lng");

  latlng1.set(0.0, 0.0);
  latlng1.move(mile, 270); // 1 mile, due west
  EQ_001(latlng1.lat, 0.0, "latlng1.move(mile,270).lat");
  EQ_001(latlng1.lng, -0.016655, "latlng1.move(mile,270).lng");

  latlng1.set(0.0, 0.0);
  latlng1.move(mile2, 45);  // 2 miles, NE
  EQ_001(latlng1.lat, 0.016655, "latlng1.move(mile2,45).lat");
  EQ_001(latlng1.lng, 0.016655, "latlng1.move(mile2,45).lng");

  Geo::LatLng latlng0(0.0,0.0);
  dtw  = latlng0.distance(latlng0);
  EQ_001(dtw, 0.0, "latlng0.distance(latlng0)");
}

void test_reaches() {
  char buf[MsgSize];
  PRINTLN("test: reaches");
  Geo::LatLng wpt0(0.0,0.0);

  int radius = 100; // meters
  double dist[] = { 45, 60, 85 };
  bool results[3];
  
  // we should reach targets to the N/E/S/W for 60 and 85 nautical miles, but not for 45
  char dir1[][2] = { "N", "E", "S", "W" };
  Geo::LatLng wpt1[] = { Geo::LatLng(1,0), Geo::LatLng(0,1), Geo::LatLng(-1,0), Geo::LatLng(0,-1) };
  double heading1[] = {  0, 90, 180, 270 };

  for (int ii = 0; ii < 4; ii++) {
    for (int jj = 0; jj < 3; jj++) {
      results[jj] = wpt0.reaches(dist[jj] * NauticalMile, heading1[ii], wpt1[ii], radius);
    }
    fmtString(buf, "reaches (%s): %s", dir1[ii], (!results[0] && results[1] && results[2]) ? "pass": "FAIL");
    PRINTLN(buf);
  }

  // we should reach targets to the NE/SE/SW/NW only for 85 nautical miles
  char dir2[][3] = { "NE", "SE", "SW", "NW" };
  Geo::LatLng wpt2[] = { Geo::LatLng(1,1), Geo::LatLng(-1,1), Geo::LatLng(-1,-1), Geo::LatLng(1,-1) };
  double heading2[] = {  45, 135, 225, 315 };

  for (int ii = 0; ii < 4; ii++) {
    for (int jj = 0; jj < 3; jj++) {
      results[jj] = wpt0.reaches(dist[jj] * NauticalMile, heading2[ii], wpt2[ii], radius);
    }
    fmtString(buf, "reaches (%s): %s", dir2[ii], (!results[0] && !results[1] && results[2]) ? "pass": "FAIL");
    PRINTLN(buf);
  }

  // we should never reach targets to the N/E/S/W using diagonal headings (heading2)
  for (int ii = 0; ii < 4; ii++) {
    bool results[3];
    for (int jj = 0; jj < 3; jj++) {
      results[jj] = wpt0.reaches(dist[jj] * NauticalMile, heading2[ii], wpt1[ii], radius);
    }
    fmtString(buf, "never reaches (%s): %s", dir1[ii], (!results[0] && !results[1] && !results[2]) ? "pass": "FAIL");
    PRINTLN(buf);
  }
  
  // similarly, we should never reach targets to the NE/SE/SW/NW using manhattan headings (heading1)
  for (int ii = 0; ii < 4; ii++) {
    bool results[3];
    for (int jj = 0; jj < 3; jj++) {
      results[jj] = wpt0.reaches(dist[jj] * NauticalMile, heading1[ii], wpt2[ii], radius);
    }
    fmtString(buf, "never reaches (%s): %s", dir2[ii], (!results[0] && !results[1] && !results[2]) ? "pass": "FAIL");
    PRINTLN(buf);
  }
}

void test() {
  PRINTLN("test: begin");
  test_formating();
  test_conversion();
  test_distances();
  test_moves();
  test_reaches();
  PRINTLN("test: end");
}

#endif
