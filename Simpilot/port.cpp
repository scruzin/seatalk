/*
  Description:
    Portability functions used by Simpilot for Arduino/standalone portability.
    Also includes some utility functions.
    Compile -DMAIN to run standalone.

  License:
    Copyright (C) 2016 Alan Noble.

    This file is part of Simpilot. Simpilot is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Simpilot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simpilot in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "port.h"

#ifdef MAIN
#include <unistd.h>

void delay(unsigned long milliseconds) {
  sleep(milliseconds / 1000); 
}

bool inputAvailable() {
  return false;  // no-op
}

void inputLine(char* buf, size_t size) {
  *buf = '\0';   // no-op
}

void output(const char *str, size_t len) {
  // str need not be null-terminated
  for (int ii = 0; ii < len; ii++) putchar(*str++); 
}

void outputByte(int byte) {
  // only useful for debugging Seatalk
  char digits[5];
  fmtHex(digits, byte);
  for (int ii = 0; digits[ii] != '\0'; ii++) {
    putchar(digits[ii]);
  }
  putchar(' ');
}

void outputLine(const char *str) {
  // str is null-terminated
  puts(str);
}

void outputFlush() {
  putchar('\n');
}

#else

#include <Arduino.h>

bool inputAvailable() {
  return Serial.available() > 0;
}

void inputLine(char* buf, size_t size) {
  while (Serial.available() == 0);
  int len = Serial.readBytesUntil('\n', buf, size);
  buf[len] = '\0';
}

void output(const char *str, size_t len) { 
  // str need not be null-terminated; 8-bit bytes
  Serial.write((const uint8_t*)str, len); 
}

void outputByte(int byte) {
  Serial.write(byte);
}

void outputLine(const char *str) { 
  // str is null-terminated; 8-bit bytes
  Serial.write((const uint8_t*)str, strlen(str));
  Serial.println("");
}

void outputFlush() {
#ifdef SEATALK
  delay(100); // small delay to allow Seatalk bus to return HIGH
#endif
}

void setup() {
#ifdef SEATALK
  Serial.begin(4800, SERIAL_9N1);
#else
  Serial.begin(4800);
#endif
  delay(2000);
#ifdef TEST
  delay(2000);
  test();
#endif
}

#endif

// handy formating functions lacking in Arduino

/*
  Name:
    itoa
  Description:
    Stringify a number in various ways.
    Pad with leading zeros if pad > 0.
    Insert an optional decimal point if precision > 0.
    When combining both, the padding specifies padding to the left of the decimal point.
  Usage:
    itoa(1234, 0, 0,...)  => "1234"
    itoa(1234, 0, 2,...)  => "12.34"
    itoa(1234, 0, 4,...)  => ".1234"
    itoa(1234, 0, 10,...) => ".1234"
    itoa(1234, 5, 0,...)  => "01234"
    itoa(1234, 3, 2,...)  => "012.34"
 */
char* itoa(long num, int pad, int precision, char* buf, size_t* len) {
  // stringify a number using base 10
  char const digits[] = "0123456789";
  char* buf_p = buf;
  int sign = 0;
  // handle sign
  if (num < 0) {
    num = -num;
    *buf_p++ = '-';
    sign = 1;
  }
  *len = sign;
  pad += sign; // allow for sign

  // calculate buf len
  long tmp = num;
  do {
    buf_p++;
    (*len)++;
    tmp /= 10;
  } while (tmp);

  // allow for decimal point and padding
  if (precision > 0) {
     buf_p++;
     (*len)++;
     // also allow for for decimal point & fractional part of number
     pad += precision + 1;
  }
  if (pad > *len) {
    buf_p += (pad - *len);
    *len += (pad - *len);
  }
  if (precision > *len - sign - 1) {
    precision = *len - sign - 1;
  }
  char* point_p = (precision > 0) ? buf_p - precision - 1: NULL;

  // work backwards, inserting digits and optional decimal point
  *buf_p = '\0';
  do {
    buf_p--;
    if (buf_p == point_p) {
      *buf_p-- = '.';
    }
    *buf_p = digits[num % 10];
    num /= 10;
  } while (num);
  
  // finally, pad if necessary, without clobbering the sign
  while (buf_p != buf + sign) {
    buf_p--;
    if (buf_p == point_p) {
      *buf_p = '.';
    } else {
      *buf_p = '0';
    }
  } 
  return buf;
}

/*
  Name:
    fmtString
  Description:
    Format a string, producing null-terminated output.
    Supports the following subset of sprintf:
    %c %s %d %0Xd %.Yf %0X.Yf, where X is leading zero padding before decimal point and Y is floating-poing precision.
    Like printf, %f always results in a leading zero.
  Limitations:
    Many! In particular, don't use the default %f for floats larger than 2147, otherwise the long integer will overflow.
*/  
int fmtString(char * out, const char* str, ...) {
  // format a va_arg string to the output string, returning count of substituted items
  int ii, pad, precision, count = 0;
  char cc, *ss;
  long num;
  size_t item_len, len = 0;
  
  va_list argv;
  va_start(argv, str);
  for (ii = 0; str[ii] != '\0'; ii++) {
    if (str[ii] == '%') {
      item_len = 1;
      count++;
      switch (str[++ii]) {
      case '%':
        out[len] = '%';
        break;
      case 'c': 
        out[len] = (char)va_arg(argv, int);
        break;
      case 's':
        ss = (char *)va_arg(argv, char *);
        item_len = strlen(ss);
        strcat(out, ss);
        break;
      case '0': // we support single-digit pad specifier with integers, e.g., %02d or padded floats or %06.5f
        pad = str[++ii] - '0';
        if (str[++ii] == 'd') {
          itoa((int)va_arg(argv, int), pad, 0, out + len, &item_len);
        } else if (str[ii] == '.') {
          precision = str[++ii] - '0';
          if (str[++ii] == 'f') {
            num = lround(pow(10, (double)precision) * (double)va_arg(argv, double));
            itoa(num, pad, precision, out + len, &item_len);
          }
        } else {
          return count;
        }
        break;
      case 'd':
        itoa((int)va_arg(argv, int), 0, 0, out + len, &item_len);
        break;
      case '.': // we support single-digit precision specifier with floats e.g., %.5f
        precision = str[++ii] - '0';
        if (str[++ii] == 'f') {
          num = lround(pow(10, precision) * (double)va_arg(argv, double));
          itoa(num, 1, precision, out + len, &item_len);
        } else {
          return count;
        }
        break;
      case 'f': // default %f to 6 digits after decimal point - the double cannot exceed 2147!
        num = lround(1000000 * (double)va_arg(argv, double));
        itoa(num, 1, 6, out + len, &item_len);
        break;
      }
      len += item_len;

    } else {
      out[len++] = str[ii];
    }
    out[len] = '\0';
  };
  va_end(argv);
  return count;
}

// format 1 or 2 bytes as hex
void fmtHex(char * out, int byte) {
  const char* HexDigits = "0123456789ABCDEF";
  if (byte >= 0x1000) {
    *out++ = HexDigits[(byte & 0xF000) >> 12];
  }
  if (byte >= 0x100) {
    *out++ = HexDigits[(byte & 0x0F00) >> 8];
  }
  *out++ = HexDigits[(byte & 0x00F0) >> 4];
  *out++ = HexDigits[byte & 0x000F];
  *out = '\0';
}

// split a time (in elapsed seconds) into hrs, mins, secs
void splitTime(double time, int* hrs, int* mins, int* secs) {
  *hrs = (int)(time / 3600);
  *mins = (int)((time - (*hrs * 3600)) / 60);
  *secs = (int)(time - (*hrs * 3600) - (*mins * 60));
}
