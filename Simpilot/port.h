/*
  Description:
    Portability functions used by Simpilot.
    #defines are defined here too.

  License:
    Copyright (C) 2016 Alan Noble.

    This file is part of Simpilot. Simpilot is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Simpilot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simpilot in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/
#include <stdarg.h>

#ifndef PORT_H
#define PORT_H 1

#define MAX(A,B) ((A)>(B)?A:B)
#define MASK(NUM,BITS,SHIFT) (((NUM)>>(SHIFT))&(2^(BITS)-1))
#define M_TO_FT(M) ((M) * 3.2808399)
#define M_TO_FA(m) ((m) * 0.546807)

//#define TEST           // compile tests

#ifdef MAIN
void delay(size_t milliseconds);
#else
#include <Arduino.h> // for delay
#define NMEA0183     // compile for NMEA 0183
//#define SEATALK      // compile for Seatalk
#endif

bool inputAvailable();
void inputLine(char *, size_t);
void output(const char*, size_t);
void outputByte(int);
void outputLine(const char*);
void outputFlush();
char* itoa(long, int, int, char*, size_t*);
int fmtString(char*, const char*, ...);
void fmtHex(char *, int);
void splitTime(double, int*, int*, int*);
#ifdef TEST
extern void test();
#endif
#endif
