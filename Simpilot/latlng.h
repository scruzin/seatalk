/*
  Description:
    Geospatial functions used by Simpilot.

  License:
    Copyright (C) 2016 Alan Noble.

    This file is part of Simpilot. Simpilot is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Simpilot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simpilot in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/
#ifndef LATLNG_H
#define LATLNG_H 1

namespace Geo {
  double toRadians(double deg);
  double fromRadians(double rad);
  double toMeters(double distance, char units);
  double fromMeters(double distance, char units);
  double toMps(double speed, char units);
  double fromMps(double speed, char units);

  class LatLng {
  public:
    LatLng() {
      lat = 0;
      lng = 0;
    }
    LatLng(double lat_, double lng_) {
      lat = lat_;
      lng = lng_;
    }
    inline LatLng& operator=(const LatLng& other) {
      lat = other.lat;
      lng = other.lng;
    }
    inline void set(double lat_, double lng_) {
      lat = lat_;
      lng = lng_;
    }

    void parts(int* lat_min, double* lat_deg, char* lat_dir, int* lng_min, double* lng_deg, char* lng_dir);
    double distance(LatLng& other);
    double crossTrackDistance(LatLng& orig, LatLng& dest);
    double bearing(LatLng& other);
    void move(double distance, double heading);
    bool reaches(double distance, double heading, LatLng& target, int radius);

    double lat;
    double lng;
  };

}
#endif

