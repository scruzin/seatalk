/*
  Description:
    Simpilot's header file.
    Defines Simulation class, message classes and related types.

  License:
    Copyright (C) 2016 Alan Noble.

    This file is part of Simpilot. Simpilot is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Simpilot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simpilot in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/
#include "port.h"
#include "latlng.h"

#define MaxMsg 256                   // max message length
#define MaxCommand 256               // max command length
#define MaxId 16                     // max identifier length, e.g., waypoint identifier

#define DefaultSpeed 5.0             // default speed in knots
#define DefaultDepth 10.0            // default depth in meters
#define DefaultTWS 15.0              // default TWS in knots
#define DefaultAWS 17.0              // default AWS in knots
#define DefaultArrivalRadius 100     // default waypoint arrival radius in meters

#define GpsNumSatellites 4           // GPS number of satellites
#define GpsHorizontalDilution 1.0    // GPS horizontal dilution (dilution of precision)
#define GpsAntennaAltitude 5.0       // GPS antenna altitude (meters)
#define GpsGeoidalSeparation 0.0     // GPS geoidal separation (meters)
#define GpsDiffStationID 0           // GPS differential station ID

#define VERSION "Simpilot v1.0"
#define COPYRIGHT "Copyright (C) 2016 Alan Noble"
#define LOG(LEVEL, FMT, ...) if (_log_level >= (LEVEL)) { char buf[MaxMsg]; fmtString(buf, #LEVEL ": " FMT, ##__VA_ARGS__); outputLine(buf); }

namespace Simpilot {
  enum LogLevel {
    Error = 0,
    Command = 1,
    Warning = 2,
    Info = 3,
    Debug = 4
  };

  enum PilotMode {
    PilotStandby = 'S',
    PilotHeading = 'H',
    PilotWaypoint = 'W',
    PilotTrack = 'T',
    PilotWindvane = 'V',
    PilotLoop = 'L'
  };

  enum DistanceUnits {
    Meters = 'M',
    Kilometers = 'K',
    NauticalMiles = 'N',
  };

  enum SpeedUnits {
    Mps = 'M',
    Kph = 'K',
    Knots = 'N',
  };

  enum HeadingUnits {
    HeadingMagnetic = 'M',
    HeadingTrue = 'T',
  };

  enum WaypointStatus {
    WaypointNotSet = -1,
    WaypointActive = 0,
    WaypointArrived = 1,
    WaypointPassed = 2
  };

  enum SteerDirection {
    SteerLeft = 'L',
    SteerRight = 'R'
  };

  typedef struct {
    char id[MaxId];
    double lat;
    double lng;
  } Waypoint;

  class Simulation {
  private:
    // state 
    PilotMode _pilot_mode;
    double _total_time;            // Time Since Start (TSS) in seconds
    double _total_distance;        // Distance Since Start (DSS) in meters
    Geo::LatLng* _init_position;   // position at very start of simulation
    Geo::LatLng _orig_position;    // position at start of a waypoint leg
    Geo::LatLng _current_position;
    double _current_speed;
    double _current_heading;
    double _mag_variation;
    Waypoint * _init_waypoint;
    Waypoint * _current_waypoint;
    WaypointStatus _waypoint_status;
    Waypoint * _waypoints; 
    int _n_waypoints;
    bool _running;

    // configuration
    const char * _protocol;
    int _log_level;
    int _timescale;
    int _period;
    DistanceUnits _distance_units;
    SpeedUnits _speed_units;
    HeadingUnits _heading_units;

  public:
    Simulation();

    inline bool running() {
      return _running;
    }
    inline void setRunning(bool running) {
      _running = running;
       LOG(Command, "sim %s", running ? "on" : "off");
       if (_running) {
         LOG(Info, VERSION);
         LOG(Info, COPYRIGHT);
         LOG(Info, "PRT=%s, POS=%.6f,%.6f, MOD=%c", _protocol, _current_position.lat, _current_position.lng, _pilot_mode);
       }   
    }
    inline void reset() {
      LOG(Command, "sim reset");
      _total_time = 0;
      _total_distance = 0;
      if (_init_position != NULL) {
        _current_position = *_init_position;
      }
      if (_init_waypoint != NULL) {
        gotoWaypoint(_init_waypoint->id, -1);
      }
    }
    inline void setMode(PilotMode mode) {
      _pilot_mode = mode;
      LOG(Command, "set mode %c", _pilot_mode);
    }
    inline void setPosition(double lat, double lng) {
      if (_init_position == NULL) {
        _init_position = new Geo::LatLng(lat, lng);
      }
      _current_position.set(lat, lng);
      LOG(Command, "set position %.6f,%.6f", lat, lng);
    }
    inline void setPositionToWaypoint(Waypoint * wpt) {
      if (_init_position == NULL) {
        _init_position = new Geo::LatLng(wpt->lat, wpt->lng);
      }
      _current_position.set(wpt->lat, wpt->lng);
      LOG(Command, "set position %s (%.6f,%.6f)", wpt->id, wpt->lat, wpt->lng);
    }
    inline void setSpeed(double speed) {
      _current_speed = speed;
      LOG(Command, "set speed %.1f", speed);
    }
    inline void setHeading(double heading) {
      _current_heading = heading;
      LOG(Command, "set heading %.1f", heading);
      // if called after the simulation has started, we switch to pilot Heading mode
      if (_running && _pilot_mode != PilotHeading) {
        setMode(PilotHeading);
      }
    }
    inline void setMagVariation(double variation) {
      _mag_variation = variation;
      LOG(Command, "set magvariation %.1f", variation);
    }

    // configuration
    // NB: protocol is configured at compilation time, not run time
    inline void setLogLevel(int level) {
      _log_level = level;
      LOG(Command, "sim logging %d", level);
    }
    inline void setTimescale(int timescale) {
      _timescale = timescale;
      LOG(Command, "sim timescale %d", timescale);
    }
    inline void setPeriod(int period) {
      _period = period;
      LOG(Command, "sim period %d", period);
    }
    inline void setDistanceUnits(DistanceUnits units) {
      _distance_units = units;
      LOG(Command, "sim units distance %c", units);
    }
    inline void setSpeedUnits(SpeedUnits units) {
      _speed_units = units;
      LOG(Command, "sim units speed %c", units);
    }
    inline void setHeadingUnits(HeadingUnits units) {
      _heading_units = units;
      LOG(Command, "sim units heading %c", units);
    }

    // waypoints
    int getWaypointIndex(const char * id);
    bool addWaypoint(const char * id, double lat, double lng);
    bool getWaypoint(const char * id, int ix, Waypoint ** wpt);
    bool gotoWaypoint(const char * id, int ix);
    bool nextWaypoint(const char * id);
    bool waypointPassed(double distance);

    // motion
    void maintainHeading(double* time, double* distance);
    void maintainTrack(double* time, double* distance);

    // output
    void send();

    void loop();
  };

  // Message types:
  //   EnvMessage: environmental message (depth, wind, etc.)
  //   PositionMessage: current position
  //   VectorMessage: current speed & heading
  //   NavMessage: current waypoint info (only when goto "on")
  //   PilotStatusMessage: current pilot status (only when pilot is "on")
  // Message member function implementions are in nmea0183.cpp and seatalk.cpp
  class Message {
  protected:
    void send() {};
  };

  class EnvMessage : public Message {
  public:
    EnvMessage();
    void send();

    double depth;
    double tws;
    double aws;
  };

  class PositionMessage : public Message {
  public:
    PositionMessage();
    void send();

    double time;
    Geo::LatLng position;
  };

  class VectorMessage : public Message {
  public:
    VectorMessage();
    void send();

    double speed;        
    SpeedUnits speed_units;
    double heading;
    double mag_heading;
  };

  class NavMessage : public Message {
  public:
    NavMessage();
    void send();

    double xte_magnitude;
    DistanceUnits xte_units;
    SteerDirection steer;
    const char * orig_id;
    const char * dest_id;
    Geo::LatLng orig;
    Geo::LatLng dest;
    double dtw;             // distance to waypoint
    double btw;             // bearing to waypoint
    double stw;             // speed to waypoint
    WaypointStatus wpt_status;
  };

  class PilotStatusMessage : public Message {
  public:
    PilotStatusMessage();
    void send();

    PilotMode mode;
    double xte_magnitude;
    DistanceUnits xte_units;
    SteerDirection steer;
    const char * wpt_id;
    WaypointStatus wpt_status;
    double heading;
    double orig_to_dest_bearing;
    double pos_to_dest_bearing;
    double heading_to_steer;
  };

  bool command(Simulation * sim, const char * cmd);
} // end Simpilot namespace

