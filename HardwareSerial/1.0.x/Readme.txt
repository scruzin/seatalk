Readme

This directory contains Nick Gammon's modified version of HardwareSerial for 9-bit serial communications.
This versions works for version 1.0.x for the Arduino IDE.
See this Arduino forum post for more info: http://forum.arduino.cc/index.php?topic=91377.0
